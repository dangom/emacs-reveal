The software emacs-reveal embeds different resources and sub-projects
under different license terms, summarized as follows.

For novel content created for emacs-reveal, the REUSE best practices
are applied, see: https://reuse.software/

As required for REUSE, the subdirectory LICENSES contains license
terms with their identifiers.  Those identifiers are included in
source files to clarify their license terms.  Some files do not
include license identifiers but come with an additional file whose
name ends in ".license"; that latter file either points to a license
or embeds usage terms.

For projects included in subdirectories, each subdirectory (or
sub-subdirectory in case of reveal.js-quiz) contains its original
LICENSE (ignoring REUSE):

- Released under GPL-3.0-or-later: org-reveal, reveal.js-jump-plugin,
  Reveal.js-TOC-Progress
- Released under MIT license: reveal.js, reveal.js-plugins, reveal.js-quiz
